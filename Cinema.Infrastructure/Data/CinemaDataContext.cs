﻿using System.Data.Entity;
using Cinema.Infrastructure.Domains;

namespace Cinema.Infrastructure.Data
{
	public class CinemaDataContext : DbContext
	{
		public DbSet<MovieDomain> Movies { get; set; }
		public DbSet<SeanceDomain> Seances { get; set; }
		public DbSet<OrderDomain> Orders { get; set; }
	}
}