﻿using System;
using System.Linq;
using Cinema.Infrastructure.Domains;

namespace Cinema.Infrastructure.Data
{
	public interface IRepository<T> : IDisposable
		where T : BaseDomain
	{
		IQueryable<T> Table { get; }
		T Get(int id);
		void Add(T domain);
		void Update(T domain);
		void Delete(int id);
	}
}