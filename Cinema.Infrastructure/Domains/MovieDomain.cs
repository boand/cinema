﻿using System.Collections.Generic;

namespace Cinema.Infrastructure.Domains
{
	public class MovieDomain : BaseDomain
	{
		public string Title { get; set; }

		public virtual ICollection<SeanceDomain> Seances { get; set; }
	}
}