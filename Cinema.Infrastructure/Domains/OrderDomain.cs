﻿using System;

namespace Cinema.Infrastructure.Domains
{
	public class OrderDomain : BaseDomain
	{
		public int AmountTickets { get; set; }
		public DateTime SaleTime { get; set; }

		public int SeanceId { get; set; }
		public virtual SeanceDomain Seance { get; set; }
	}
}