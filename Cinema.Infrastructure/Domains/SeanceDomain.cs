﻿using System;
using System.Collections.Generic;

namespace Cinema.Infrastructure.Domains
{
	public class SeanceDomain : BaseDomain
	{
		public DateTime StartTime { get; set; }

		public int MovieId { get; set; }
		public virtual MovieDomain Movie { get; set; }

		public virtual ICollection<OrderDomain> Orders { get; set; }
	}
}