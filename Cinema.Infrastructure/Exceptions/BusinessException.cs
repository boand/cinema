﻿using System;

namespace Cinema.Infrastructure.Exceptions
{
	public class BusinessException : Exception
	{
		public BusinessException(string messageForUser) : base(messageForUser)
		{
		}
	}
}