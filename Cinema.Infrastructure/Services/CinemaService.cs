﻿using System;
using System.Data.Entity;
using System.Linq;
using Cinema.Infrastructure.Data;
using Cinema.Infrastructure.Domains;
using Cinema.Infrastructure.Exceptions;
using Cinema.Infrastructure.Resources;
using Cinema.Infrastructure.Services.Interfaces;

namespace Cinema.Infrastructure.Services
{
	public class CinemaService : IMovieService, ISeanceService, IOrderService
	{
		private readonly IRepository<MovieDomain> _movieRepository;
		private readonly IRepository<OrderDomain> _orderRepository;
		private readonly IRepository<SeanceDomain> _seanceRepository;

		public CinemaService(IRepository<MovieDomain> movieRepository, IRepository<OrderDomain> orderRepository,
			IRepository<SeanceDomain> seanceRepository)
		{
			_movieRepository = movieRepository;
			_orderRepository = orderRepository;
			_seanceRepository = seanceRepository;
		}

		public MovieDomain[] GetAllMovies()
		{
			return GetMovieQuery().ToArray();
		}

		public MovieDomain GetMovie(int id)
		{
			return GetMovieQuery().FirstOrDefault(x => x.Id == id);
		}

		public MovieDomain AddMovie(string title)
		{
			if (string.IsNullOrWhiteSpace(title))
				throw new BusinessException(Resource.MovieEmptyTitle);

			var movie = new MovieDomain {Title = title};
			_movieRepository.Add(movie);

			return movie;
		}

		public IQueryable<OrderDomain> GetOrdersOnSeance(int seanceId)
		{
			return _orderRepository.Table.Where(x => x.SeanceId == seanceId);
		}

		public OrderDomain AddOrder(int seanceId, int amountTickets)
		{
			if (amountTickets < 1)
				throw new BusinessException(Resource.OrderLowAmount);

			var seance = _seanceRepository.Get(seanceId);
			if (seance == null)
				throw new BusinessException(Resource.SeanceNotExist);

			if (seance.StartTime < DateTime.Now)
				throw new BusinessException(Resource.OrderStop);

			var order = new OrderDomain
			{
				SeanceId = seance.Id,
				AmountTickets = amountTickets,
				SaleTime = DateTime.Now
			};
			_orderRepository.Add(order);

			return order;
		}

		public SeanceDomain AddSeance(int movieId, DateTime startTime)
		{
			var movie = _movieRepository.Get(movieId);
			if (movie == null)
				throw new BusinessException(Resource.MovieNotExist);

			var seance = new SeanceDomain {MovieId = movie.Id, StartTime = startTime};
			_seanceRepository.Add(seance);

			return seance;
		}

		public SeanceDomain GetSeance(int id)
		{
			return _seanceRepository.Get(id);
		}

		private IQueryable<MovieDomain> GetMovieQuery()
		{
			return _movieRepository.Table.Include(x => x.Seances.Select(y => y.Orders));
		}
	}
}