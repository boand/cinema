﻿using Cinema.Infrastructure.Domains;

namespace Cinema.Infrastructure.Services.Interfaces
{
	public interface IMovieService
	{
		MovieDomain[] GetAllMovies();
		MovieDomain GetMovie(int id);
		MovieDomain AddMovie(string title);
	}
}