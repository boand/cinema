﻿using System.Linq;
using Cinema.Infrastructure.Domains;

namespace Cinema.Infrastructure.Services.Interfaces
{
	public interface IOrderService
	{
		IQueryable<OrderDomain> GetOrdersOnSeance(int seanceId);
		OrderDomain AddOrder(int seanceId, int amountTickets);
	}
}