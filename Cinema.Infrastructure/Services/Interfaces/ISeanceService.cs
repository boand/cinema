﻿using System;
using Cinema.Infrastructure.Domains;

namespace Cinema.Infrastructure.Services.Interfaces
{
	public interface ISeanceService
	{
		SeanceDomain GetSeance(int id);
		SeanceDomain AddSeance(int movieId, DateTime startTime);
	}
}