﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Threading;
using Cinema.Seller.Models;
using Cinema.Seller.ModelsWpf;
using Microsoft.Rest;
using Newtonsoft.Json;

namespace Cinema.Seller
{
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			FillSeanceList();

			var dispatcherTimer = new DispatcherTimer();
			dispatcherTimer.Tick += (o, args) => FillSeanceList();
			dispatcherTimer.Interval = new TimeSpan(0, 0, 20);
			dispatcherTimer.Start();
		}

		private async void FillSeanceList()
		{
			try
			{
				var movies = await SellerClient.GetInstance().MovieOperations.GetMoviesAsync();
				var items = movies
					.SelectMany(x =>
						x.Seances.Select(y => new SeanceWpf {Id = y.Id, Title = x.Title, StartTime = y.StartTime}))
					.OrderBy(x => x.StartTime);

				SeanceList.ItemsSource = items;
			}
			catch (Exception ex)
			{
				ExceptionHandler(ex);
			}
		}

		private async void Button_Click(object sender, RoutedEventArgs e)
		{
			if (SeanceList.SelectedItem is SeanceWpf seance)
				try
				{
					var amountTickets = AmountTickets.Value;
					var newOrder = new NewOrder
					{
						SeanceId = seance.Id,
						AmountTickets = amountTickets
					};
					await SellerClient.GetInstance().Order.PostWithHttpMessagesAsync(newOrder);
					MessageBox.Show($"Билеты куплены: {amountTickets} шт.");
				}
				catch (Exception ex)
				{
					ExceptionHandler(ex);
				}
			else
				MessageBox.Show("Необходимо выбрать сеанс");
		}

		private void ExceptionHandler(Exception ex)
		{
			if (ex is HttpOperationException e && e.Response.StatusCode == HttpStatusCode.BadRequest)
			{
				dynamic response = JsonConvert.DeserializeObject(e.Response.Content);
				string message = response.Message;
				MessageBox.Show(message);
			}
			else
			{
				MessageBox.Show("Произошла ошибка на сервере. Пожалуйста повторите попытку");
			}
		}
	}
}