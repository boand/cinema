﻿using System;

namespace Cinema.Seller.ModelsWpf
{
	public class SeanceWpf
	{
		public int? Id { get; set; }
		public string Title { get; set; }
		public DateTime? StartTime { get; set; }
	}
}