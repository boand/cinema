﻿using System.Threading.Tasks;
using Cinema.Seller.Models;
using Microsoft.Rest;
using Newtonsoft.Json.Linq;

namespace Cinema.Seller
{
	public partial class SellerClient
	{
		private static SellerClient _instance;

		public static SellerClient GetInstance()
		{
			return _instance ?? (_instance = new SellerClient(new BasicAuthenticationCredentials()));
		}
	}

	public static partial class MovieOperationsExtensions
	{
		public static async Task<Movie[]> GetMoviesAsync(this IMovieOperations operations)
		{
			var result = await operations.GetWithHttpMessagesAsync();

			return (result.Body is JArray result1 ? result1.ToObject<Movie[]>() : null) ?? new Movie[0];
		}
	}
}