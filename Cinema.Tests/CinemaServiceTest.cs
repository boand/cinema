﻿using System;
using System.Linq;
using Cinema.Infrastructure.Domains;
using Cinema.Infrastructure.Exceptions;
using Cinema.Infrastructure.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cinema.Tests
{
	[TestClass]
	public class CinemaServiceTest
	{
		private CinemaService _cinemaService;
		private MockRepository<MovieDomain> _movieRepository;
		private MockRepository<OrderDomain> _orderRepository;
		private MockRepository<SeanceDomain> _seanceRepository;

		[TestInitialize]
		public void SetUp()
		{
			_movieRepository = new MockRepository<MovieDomain>();
			_orderRepository = new MockRepository<OrderDomain>();
			_seanceRepository = new MockRepository<SeanceDomain>();

			_cinemaService = new CinemaService(_movieRepository, _orderRepository, _seanceRepository);
		}

		[TestMethod]
		public void AddMovieTest()
		{
			_cinemaService.AddMovie("Фильм");

			Assert.AreEqual(_cinemaService.GetAllMovies().Single().Title, "Фильм");
		}

		[TestMethod]
		[DataRow("")]
		[DataRow("  ")]
		[ExpectedException(typeof(BusinessException))]
		public void AddMovieEmptyTitleTest(string title)
		{
			_cinemaService.AddMovie(title);
		}

		[TestMethod]
		public void AddSeanceTest()
		{
			_movieRepository.Add(new MovieDomain {Id = 0});
			var startTime = DateTime.Now;

			_cinemaService.AddSeance(0, startTime);

			Assert.AreEqual(_seanceRepository.Table.Single().StartTime, startTime);
		}

		[TestMethod]
		[ExpectedException(typeof(BusinessException))]
		public void AddSeanceOnMovieNotExistTest()
		{
			_cinemaService.AddSeance(0, DateTime.Now);
		}

		[TestMethod]
		public void AddOrderTest()
		{
			_seanceRepository.Add(new SeanceDomain {Id = 0, StartTime = DateTime.Now.AddDays(1)});

			_cinemaService.AddOrder(0, 2);

			Assert.AreEqual(_cinemaService.GetOrdersOnSeance(0).Single().AmountTickets, 2);
		}

		[TestMethod]
		public void AddOrderOnSeanceNotExist()
		{
			try
			{
				_cinemaService.AddOrder(0, 1);
			}
			catch (BusinessException e)
			{
				Assert.AreEqual("Сеанс не найден", e.Message);
			}
			catch (Exception e)
			{
				Assert.Fail(e.Message);
			}
		}

		[TestMethod]
		public void AddOrderLowAmountTest()
		{
			_seanceRepository.Add(new SeanceDomain {Id = 0, StartTime = DateTime.Now.AddDays(1)});

			try
			{
				_cinemaService.AddOrder(0, 0);
			}
			catch (BusinessException e)
			{
				Assert.AreEqual("Нельзя продать меньше одного билета", e.Message);
			}
			catch (Exception e)
			{
				Assert.Fail(e.Message);
			}
		}

		[TestMethod]
		public void OrderStopTest()
		{
			_seanceRepository.Add(new SeanceDomain {Id = 0, StartTime = DateTime.Now.AddDays(-1)});

			try
			{
				_cinemaService.AddOrder(0, 1);
			}
			catch (BusinessException e)
			{
				Assert.AreEqual("Продажи на сеанс закончились", e.Message);
			}
			catch (Exception e)
			{
				Assert.Fail(e.Message);
			}
		}
	}
}