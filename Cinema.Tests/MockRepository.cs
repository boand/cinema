﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cinema.Infrastructure.Data;
using Cinema.Infrastructure.Domains;

namespace Cinema.Tests
{
	public class MockRepository<T> : IRepository<T> where T : BaseDomain
	{
		public readonly List<T> InMemoryDataBase = new List<T>();

		public void Dispose()
		{
		}

		public IQueryable<T> Table => InMemoryDataBase.AsQueryable();

		public T Get(int id)
		{
			return InMemoryDataBase.FirstOrDefault(x => x.Id == id);
		}

		public void Add(T domain)
		{
			InMemoryDataBase.Add(domain);
		}

		public void Update(T domain)
		{
			throw new NotImplementedException();
		}

		public void Delete(int id)
		{
			throw new NotImplementedException();
		}
	}
}