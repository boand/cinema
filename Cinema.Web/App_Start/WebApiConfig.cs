﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;
using Cinema.Infrastructure.Exceptions;

namespace Cinema.Web
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			// Web API configuration and services
			config.Filters.Add(new HandleExceptionAttribute());

			// Web API routes
			config.MapHttpAttributeRoutes();

			config.Routes.MapHttpRoute(
				"DefaultApi",
				"api/{controller}/{id}",
				new {id = RouteParameter.Optional}
			);
		}
	}

	public class HandleExceptionAttribute : ExceptionFilterAttribute
	{
		public override void OnException(HttpActionExecutedContext context)
		{
			var ex = context.Exception;

			if (ex is BusinessException)
				context.Response = context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
		}
	}
}