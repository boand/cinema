﻿using System.Web.Mvc;

namespace Cinema.Web.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
	}
}