﻿using System.Linq;
using System.Web.Http;
using Cinema.Infrastructure.Data;
using Cinema.Infrastructure.Domains;
using Cinema.Infrastructure.Services;
using Cinema.Infrastructure.Services.Interfaces;
using Cinema.Web.Models;

namespace Cinema.Web.Controllers
{
	public class MovieController : ApiController
	{
		private readonly CinemaDataContext _dataContext = new CinemaDataContext();
		private readonly IMovieService _movieService;

		public MovieController()
		{
			_movieService = new CinemaService(new EfRepository<MovieDomain>(_dataContext),
				new EfRepository<OrderDomain>(_dataContext), new EfRepository<SeanceDomain>(_dataContext));
		}

		[HttpGet]
		public IHttpActionResult Get()
		{
			var movies = _movieService.GetAllMovies().Select(Mapper.Movie);
			return Json(movies);
		}

		[HttpPost]
		public IHttpActionResult Post([FromBody] Movie movie)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			var domain = _movieService.AddMovie(movie.Title);

			movie = Mapper.Movie(domain);

			return Created(Request.RequestUri + movie.Id.ToString(), movie);
		}

		protected override void Dispose(bool disposing)
		{
			_dataContext.Dispose();
			base.Dispose(disposing);
		}
	}
}