﻿using System.Linq;
using System.Web.Http;
using Cinema.Infrastructure.Data;
using Cinema.Infrastructure.Domains;
using Cinema.Infrastructure.Services;
using Cinema.Infrastructure.Services.Interfaces;
using Cinema.Web.Models;

namespace Cinema.Web.Controllers
{
	public class OrderController : ApiController
	{
		private readonly CinemaDataContext _dataContext = new CinemaDataContext();
		private readonly IOrderService _orderService;

		public OrderController()
		{
			_orderService = new CinemaService(new EfRepository<MovieDomain>(_dataContext),
				new EfRepository<OrderDomain>(_dataContext), new EfRepository<SeanceDomain>(_dataContext));
		}

		[HttpGet]
		public IHttpActionResult GetOrdersOnSeance(int seanceId)
		{
			var orders = _orderService.GetOrdersOnSeance(seanceId).Select(Mapper.Order);
			return Json(orders);
		}

		[HttpPost]
		public IHttpActionResult Post([FromBody] NewOrder newOrder)
		{
			var order = _orderService.AddOrder(newOrder.SeanceId, newOrder.AmountTickets);
			return Ok();
		}

		protected override void Dispose(bool disposing)
		{
			_dataContext.Dispose();
			base.Dispose(disposing);
		}
	}
}