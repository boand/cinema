﻿using System.Web.Http;
using Cinema.Infrastructure.Data;
using Cinema.Infrastructure.Domains;
using Cinema.Infrastructure.Services;
using Cinema.Infrastructure.Services.Interfaces;
using Cinema.Web.Models;

namespace Cinema.Web.Controllers
{
	public class SeanceController : ApiController
	{
		private readonly CinemaDataContext _dataContext = new CinemaDataContext();
		private readonly ISeanceService _seanceService;

		public SeanceController()
		{
			_seanceService = new CinemaService(new EfRepository<MovieDomain>(_dataContext),
				new EfRepository<OrderDomain>(_dataContext), new EfRepository<SeanceDomain>(_dataContext));
		}

		[HttpGet]
		public IHttpActionResult Get(int id)
		{
			var domain = _seanceService.GetSeance(id);
			var seance = Mapper.Seance(domain);
			return Json(seance);
		}

		[HttpPost]
		public IHttpActionResult Post([FromBody] NewSeance newSeance)
		{
			var domain = _seanceService.AddSeance(newSeance.MovieId, newSeance.StartTime);
			var seance = Mapper.Seance(domain);
			return Json(seance);
		}

		protected override void Dispose(bool disposing)
		{
			_dataContext.Dispose();
			base.Dispose(disposing);
		}
	}
}