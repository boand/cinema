﻿using System.Linq;
using Cinema.Infrastructure.Domains;
using Cinema.Web.Models;

namespace Cinema.Web
{
	public static class Mapper
	{
		public static Movie Movie(MovieDomain movie)
		{
			return new Movie
			{
				Id = movie.Id,
				Title = movie.Title,
				Seances = movie.Seances?.Select(Seance) ?? new Seance[0]
			};
		}

		public static Seance Seance(SeanceDomain seance)
		{
			return new Seance
			{
				Id = seance.Id,
				StartTime = seance.StartTime,
				CountSoldTickets = seance.Orders?.Sum(order => order.AmountTickets) ?? 0
			};
		}

		public static Order Order(OrderDomain order)
		{
			return new Order
			{
				Id = order.Id,
				SaleTime = order.SaleTime,
				AmountTickets = order.AmountTickets
			};
		}
	}
}