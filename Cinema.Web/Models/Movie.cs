﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cinema.Web.Models
{
	public class Movie
	{
		public int Id { get; set; }

		[Required(AllowEmptyStrings = false)] public string Title { get; set; }

		public virtual IEnumerable<Seance> Seances { get; set; }
	}
}