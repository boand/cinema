﻿namespace Cinema.Web.Models
{
	public class NewOrder
	{
		public int SeanceId { get; set; }
		public int AmountTickets { get; set; }
	}
}