﻿using System;

namespace Cinema.Web.Models
{
	public class NewSeance
	{
		public int MovieId { get; set; }
		public DateTime StartTime { get; set; }
	}
}