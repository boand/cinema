﻿using System;

namespace Cinema.Web.Models
{
	public class Order
	{
		public int Id { get; set; }
		public DateTime SaleTime { get; set; }
		public int AmountTickets { get; set; }
	}
}