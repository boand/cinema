﻿using System;

namespace Cinema.Web.Models
{
	public class Seance
	{
		public int Id { get; set; }

		public DateTime StartTime { get; set; }

		public int CountSoldTickets { get; set; }
	}
}